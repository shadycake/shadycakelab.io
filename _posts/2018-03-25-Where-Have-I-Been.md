---
layout: post
title: Where have I been?
---

I haven't streamed or updated this blog in a couple weeks, this is mostly because of my time being occupied by GCSEs.

But this doesn't mean I haven't been busy! This weekend I set up autodeploy on my server which I can link to any repository I want.

It works like this:
 - Whenever a branch has been updated, [Travis CI](https://travis-ci.org/) will run a flake8 on the repository, checking all the python styling.
 - However, if the branch is the master branch, it will send a request to my server through a webhook using a special token.
 - My server will then hash this token, and check it against a dictionary to get the required state file to apply.
 - It will then, using [SaltStack](https://saltstack.com/), tell my server to run a series of functions (The functions are specified in the state file mentioned earlier, meaning I can configure it for different repoistories).
 - This, in order, makes a folder, clones the repository to that folder, and builds a [Docker](https://www.docker.com/) image.
 - This image is then set to run in a container (An isolated environment for the code to run, which contains all the necessary packages).
 - The next steps rely on what the repo is, if its a webapp it will be allowed to communicate with [nginx](https://nginx.org/) which then can communicate with the outside world and be accessible, or if its a discord bot for example, it would be allowed to communicate with the outside world immediately so it can talk with the discord servers.
 
Since it was my first time doing something like this, I've learnt a lot about these technologies (CIs, Saltstack, Docker, nginx) and it ate up most of my weekend fixing errors (Don't get me wrong, I still had fun with the learning), but ultimately this will allow an infrastructure upon which I can release my services, such as a discord bot.

I will certainly be more active post-GCSEs, which end around late July. I will still try to keep this updated if I make any advancements!
