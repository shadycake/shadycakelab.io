---
layout: post
title: Hello, World!
---

Hello, World! And welcome to my new blog, where I will be keeping regular updates on projects that I am working on.

As of right now I am working on one project, which is [Save the Cave!](https://github.com/shadycake/save-the-cave) (The name is subject to change), a 2D platformer made in [Godot Engine 3](https://godotengine.org/). I occasionally stream my development on my [Twitch account](https://twitch.tv/shadycake), so follow it to keep an eye out if you're interested!

However, I plan to work on multiple projects in the future, such as:
 - A [Discord](https://discordapp.com) Bot
 - A [Twitch](https://twitch.tv) Bot
 - And more!

All of which will be open source! :tada:

Aside from these projects, I also may participate in challenges and events, such as CTFs and conferences which I will also document here. Something I am taking part in currently is [Cyber Discovery](joincyberdiscovery.com), a program for aspiring cyber security professionals, with multiple stages with increasing difficulty. I am having fun with it so far, and would highly suggest those who are interested to check it out (You must be within GCSE and A-Level years and living in England), sign-ups will re-open next year.

I hope you enjoy my blog!
