---
layout: post
title: Save the Cave! Dev Update#000
---

(Yes I started at 0)

As I started development for Save the Cave! before starting this blog, I decided to create a quick rundown of what exists thus far (The gifs in this post may take a while to load).


In order, I did:

![Save-The-Cave-Update-000_0.gif]({{ site.baseurl }}/img/posts/Save-The-Cave-Update-000_0.gif?v=1)

 - The player sprites
 - Basic platformer movement (+Collisions)
 
![Save-The-Cave-Update-000_1.gif]({{ site.baseurl }}/img/posts/Save-The-Cave-Update-000_1.gif?v=1)
 
 - Pixel perfection (The script is an AutoLoad, and can be found [here](https://gist.github.com/shadycake/df4d5e305c96507b0f32888f1a7a10ab).)
 - Sprites for tiles
 - Tilemap that joins together nicely with bitmasks and collisions

 
![Save-The-Cave-Update-000_2.gif]({{ site.baseurl }}/img/posts/Save-The-Cave-Update-000_2.gif?v=1)

 - I added multiple scenes, and a fade-in scene transition
 - I added doors which let you travel between scenes.git 

I will be posting more development updates for this and other projects in the future, with new features that I come out with. You can track the new features planned in the [Issues](https://github.com/shadycake/save-the-cave/issues) section of the repository (found [here](https://github.com/shadycake/save-the-cave)) on GitHub.
