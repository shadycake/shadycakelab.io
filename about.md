---
layout: page
title: About
permalink: /about/
---

Hi! My name is Sam, I live in London and I love messing with maths, science and programming. This website was built using [GitHub Pages](https://pages.github.com/), you can find the repository [here](https://github.com/shadycake/shadycake.github.io/).

### Favourites/What I use

 - Programming Language: [Python](https://www.python.org/)
 - OS: [Arch Linux](https://www.archlinux.org/)
 - Desktop Environment: [GNOME](https://www.gnome.org/)
 - Window Manager: [GNOME Shell](https://www.gnome.org/)
 - Editor: [Visual Studio Code](https://code.visualstudio.com/)
 - Python IDE: [Pycharm](https://www.jetbrains.com/pycharm/)
 - Game Engine: [Godot 3.0](https://godotengine.org/)
 - Android: [LineageOS](https://www.lineageos.org/)

### Find me elsewhere!

 - GitHub: [shadycake](https://github.com/shadycake)
 - Stack Overflow: [ShadyCake](https://stackoverflow.com/users/9356457/shadycake)
 - Discord: Shady#1755 (You can find me in the [Python Discord](https://pythondiscord.com/invite))
 - LinkedIn: [Sam Wedgwood](https://www.linkedin.com/in/sam-wedgwood/)
 - Twitter: [shady_cake](https://twitter.com/shady_cake)
 - Twitch: [ShadyCake37](https://twitch.tv/Shadycake37)
 - YouTube: [Sam Wedgwood](https://www.youtube.com/channel/UCOOemxS_P1vep4UbFZ5DWYQ)
 - Steam: [ShadyCake](https://steamcommunity.com/id/shadycake)
 - Brilliant: [Sam W](https://brilliant.org/profile/sam-czvqgw/)
 - Spotify: [univ](https://open.spotify.com/user/zl0rmyahcga6i6d95bjm6ls94)

### Contact me

 - [sam@altdev.me](mailto:sam@altdev.me)